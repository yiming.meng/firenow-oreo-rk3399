#
# Copyright 2014 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
add_lunch_combo rk3399_firefly_mid-userdebug
add_lunch_combo rk3399_firefly_mid
add_lunch_combo rk3399_firefly_edp_mid-userdebug
add_lunch_combo rk3399_firefly_edp_mid
add_lunch_combo rk3399_firefly_mipi_mid-userdebug
add_lunch_combo rk3399_firefly_mipi_mid
add_lunch_combo rk3399_firefly_aio_mid-userdebug
add_lunch_combo rk3399_firefly_aio_mid
add_lunch_combo rk3399_firefly_aio_lvds_mid-userdebug
add_lunch_combo rk3399_firefly_aio_lvds_mid
add_lunch_combo rk3399_firefly_aioc_mid-userdebug
add_lunch_combo rk3399_firefly_aioc_mid
add_lunch_combo rk3399_firefly_aioc_lvds_mid-userdebug
add_lunch_combo rk3399_firefly_aioc_lvds_mid
add_lunch_combo ROC_RK3399_PC-userdebug
add_lunch_combo ROC_RK3399_PC
